# Launch Interceptor Program

### Environnement requis

Compilateur requis :
- OCaml 4.03.0

Librairies requises :
- Yojson
- Biniou
- Easyformat

### Compilation

Pour compiler, il suffit de lancer le script :
```sh
$ ./build
```

### Exécution

L'appel du programme s'effectue comme suit :
```sh
$ ./decide file.json
```
