
type parameters = {
  epsilon : float; dist : float; quads : int;
  radius1 : float; radius2 : float;
  length1 : float; length2 : float;
  area1 : float; area2 : float;
  a_pts : int; b_pts : int;
  c_pts : int; d_pts : int;
  e_pts : int; f_pts : int;
  g_pts : int; n_pts : int;
  k_pts : int; q_pts : int
}

type data = {
  numpoints : int;
  points : (float * float) array;
  lcm : ((bool -> bool -> bool) array) array;
  puv : bool array;
}

let nb_cond = 15

let load_json file  =
  let open Yojson.Basic.Util in
  let to_logical s =
    let s = s |> to_string in
    if s = "ANDD" then (&&)
    else if s = "ORR" then (||)
    else (fun _ _ -> true)
  in
  let to_point p =
    match p |> to_list with
    | [x; y] -> (x |> to_float), (y |> to_float)
    | _ -> assert false
  in
  let json = Yojson.Basic.from_file file in
  let parameters = json |> member "PARAMETERS" in
  let epsilon = parameters |> member "EPSILON" |> to_float in
  let dist = parameters |> member "DIST" |> to_float in
  let radius1 = parameters |> member "RADIUS1" |> to_float in
  let radius2 = parameters |> member "RADIUS2" |> to_float in
  let length1 = parameters |> member "LENGTH1" |> to_float in
  let length2 = parameters |> member "LENGTH2" |> to_float in
  let area1 = parameters |> member "AREA1" |> to_float in
  let area2 = parameters |> member "AREA2" |> to_float in
  let quads = parameters |> member "QUADS" |> to_int in
  let a_pts = parameters |> member "A_PTS" |> to_int in
  let b_pts = parameters |> member "B_PTS" |> to_int in
  let c_pts = parameters |> member "C_PTS" |> to_int in
  let d_pts = parameters |> member "D_PTS" |> to_int in
  let e_pts = parameters |> member "E_PTS" |> to_int in
  let f_pts = parameters |> member "F_PTS" |> to_int in
  let g_pts = parameters |> member "G_PTS" |> to_int in
  let n_pts = parameters |> member "N_PTS" |> to_int in
  let k_pts = parameters |> member "K_PTS" |> to_int in
  let q_pts = parameters |> member "Q_PTS" |> to_int in
  let numpoints = json |> member "NUMPOINTS" |> to_int in
  let points = List.map to_point (json |> member "points" |> to_list) in
  let puv = List.map to_bool (json |> member "PUV" |> to_list) in
  let lcm = List.map (fun (s, l) ->
      (int_of_string s), (List.map to_logical (l |> to_list))
    ) (json |> member "LCM" |> to_assoc) in
  { epsilon; dist; radius1; radius2; length1; length2; area1; area2; quads;
    a_pts; b_pts; c_pts; d_pts; e_pts; f_pts; g_pts; n_pts; k_pts; q_pts },
  { numpoints; points = Array.of_list points; puv = Array.of_list puv;
    lcm = Array.init nb_cond (fun i -> Array.of_list (List.assoc i lcm)) }

let params, data =
  try load_json Sys.argv.(1)
  with _ -> Printf.eprintf "input error\n"; exit 1

let pi = 3.1415926535

let compare a b =
  if abs_float (a -. b) < 0.000001 then 0
  else if a < b then -1  else 1

let quadrant (x, y) =
  if y >= 0. then (if x >= 0. then 1 else 2)
             else (if x <= 0. then 3 else 4)

let distance2 (x1, y1) (x2, y2) =
  let x = x2 -. x1 in
  let y = y2 -. y1 in
  x *. x +. y *. y

let distance p1 p2 =
  sqrt (distance2 p1 p2)

let coincide p1 p2 =
  compare (distance p1 p2) 0. = 0

let area_triangle p1 p2 p3 = (* Using Heron's formula *)
  let a = distance p1 p2 in
  let b = distance p1 p3 in
  let c = distance p2 p3 in
  let s = (a +. b +. c) /. 2. in
  sqrt (s *. (s -. a) *. (s -. b) *. (s -. c))

let angle p1 p2 p3 = (* Using the law of cosines *)
  let a2 = distance2 p2 p1 in
  let b2 = distance2 p2 p3 in
  let a = sqrt a2 in
  let b = sqrt b2 in
  if compare a 0. = 0 || compare b 0. = 0 then None
  else
    let c2 = distance2 p1 p3 in
    let cos = (a2 +. b2 -. c2) /. (2. *. a *. b) in
    let cos = if cos < -1. then -1. else if cos > 1. then 1. else cos in
    Some (acos cos)

let angle_below p1 p2 p3 pa epsilon =
  match angle p1 p2 p3 with
  | None -> false
  | Some a -> compare a (pa -. params.epsilon) < 0 ||
		compare a (pa +. params.epsilon) > 0

let in_or_on_circle p1 p2 p3 r =
  let d12 = distance2 p1 p2 in
  let d13 = distance2 p1 p3 in
  let d23 = distance2 p2 p3 in
  let d, (x1, y1), (x2, y2), p3 = (* choose the two furthest points *)
    if d12 > d13 && d12 > d23 then sqrt d12, p1, p2, p3
    else if d13 > d12 && d13 > d23 then sqrt d13, p1, p3, p2
    else sqrt d23, p2, p3, p1 in
  let hd = d /. 2. in (* half the length of segment p1p2 *)
  if hd > r then false (* the two furthest points have a distance > diameter -> false *)
  else
    let xm = (x1 +. x2) /. 2. in (* coordinates of the middle *)
    let ym = (y1 +. y2) /. 2. in (* of segment p1p2 *)
    let dmc = sqrt (r *. r -. hd *. hd) in (* distance between m and c *)
    let ndx = (y1 -. y2) /. d in (* normalized direction of the *)
    let ndy = (x2 -. x1) /. d in (* line going through m and c *)
    let cx1 = xm +. dmc *. ndx in (* coordinate of center of circle 1 *)
    let cy1 = ym +. dmc *. ndy in
    let cx2 = xm -. dmc *. ndx in (* coordinate of center of circle 2 *)
    let cy2 = ym -. dmc *. ndy in
    compare (distance (cx1, cy1) p3) r <= 0 ||
      compare (distance (cx2, cy2) p3) r <= 0

let distance_line ((x1, y1) as p1) ((x2, y2) as p2) (x0, y0) = (* using the area *)
  abs_float ((y2 -. y1) *. x0 -. (x2 -. x1) *. y0 +. x2 *. y1 -. y2 *. x1) /.
    (distance p1 p2)

let exists_Ncons a n p =
  let rec loop i =
    if i >= Array.length a - (n - 1) then false
    else (p a n i || loop (i+1))
  in
  loop 0

let exists_2sepcons a s p =
  let rec loop i =
    if i >= Array.length a - (1 + s) then false
    else (p a.(i) a.(i+1+s) || loop (i+1))
  in
  loop 0

let exists_3sepcons a s1 s2 p =
  let rec loop i = 
    if i >= Array.length a - (2 + s1 + s2) then false
    else (p a.(i) a.(i+1+s1) a.(i+2+s1+s2) || loop (i+1))
  in
  loop 0

let exists_2cons a p = exists_2sepcons a 0 p

let exists_3cons a p = exists_3sepcons a 0 0 p

let decide () =
  
  let cmv = Array.make nb_cond false in

  cmv.(0) <- exists_2cons data.points (fun p1 p2 ->
	       compare (distance p1 p2) params.length1 > 0);

  cmv.(1) <- exists_3cons data.points (fun p1 p2 p3 ->
	       not (in_or_on_circle p1 p2 p3 params.radius1));

  cmv.(2) <- exists_3cons data.points (fun p1 p2 p3 ->
	       angle_below p1 p2 p3 pi params.epsilon);

  cmv.(3) <- exists_3cons data.points (fun p1 p2 p3 ->
	       compare (area_triangle p1 p2 p3) params.area1 > 0);

  cmv.(4) <- exists_Ncons data.points params.q_pts (fun a n s ->
	       let quads = Array.make 4 0 in
	       let rec loop i =
		 if Array.fold_left (+) 0 quads > params.quads then true
		 else if i >= s + n then false
		 else begin quads.(quadrant a.(i) - 1) <- 1; loop (i+1) end
	       in
	       loop s);

  cmv.(5) <- exists_2cons data.points (fun (x1, _) (x2, _) ->
	       compare x2 x1 < 0);

  cmv.(6) <- data.numpoints >= 3 &&
	     exists_Ncons data.points params.n_pts (fun a n s ->
	       let p1 = a.(s) in
	       let p2 = a.(s+n-1) in
	       if coincide p1 p2 then
		 let rec loop i =
		   if i >= s + n then false
		   else (compare (distance p1 a.(i)) params.dist > 0
			 || loop (i+1))
		 in
		 loop s
	       else
		 let rec loop i =
		   if i >= s + n then false
		   else (compare (distance_line p1 p2 a.(i)) params.dist > 0
			 || loop (i+1))
		 in
		 loop s);

  cmv.(7) <- data.numpoints >= 3 &&
	     exists_2sepcons data.points params.k_pts (fun p1 p2 ->
	       compare (distance p1 p2) params.length1 > 0);

  cmv.(8) <- data.numpoints >= 5 &&
	     exists_3sepcons data.points params.a_pts params.b_pts (
	       fun p1 p2 p3 -> not (in_or_on_circle p1 p2 p3 params.radius1));

  cmv.(9) <- data.numpoints >= 5 &&
	     exists_3sepcons data.points params.c_pts params.d_pts (
	       fun p1 p2 p3 -> angle_below p1 p2 p3 pi params.epsilon);

  cmv.(10) <- data.numpoints >= 5 &&
	      exists_3sepcons data.points params.e_pts params.f_pts (
	        fun p1 p2 p3 ->
		  compare (area_triangle p1 p2 p3) params.area1 > 0);

  cmv.(11) <- data.numpoints >= 3 &&
	      exists_2sepcons data.points params.g_pts (
		fun (x1, _) (x2, _) -> compare x2 x1 < 0);

  cmv.(12) <- data.numpoints >= 3 && cmv.(7) &&
	      exists_2sepcons data.points params.k_pts (
		fun p1 p2 -> compare (distance p1 p2) params.length2 < 0);

  cmv.(13) <- data.numpoints >= 5 && cmv.(8) &&
	      exists_3sepcons data.points params.a_pts params.b_pts (
	        fun p1 p2 p3 -> in_or_on_circle p1 p2 p3 params.radius2);

  cmv.(14) <- data.numpoints >= 5 && cmv.(10) &&
	      exists_3sepcons data.points params.e_pts params.f_pts (
	        fun p1 p2 p3 ->
		  compare (area_triangle p1 p2 p3) params.area2 < 0);

  let pum = Array.init nb_cond (fun i ->
    Array.init nb_cond (fun j -> i = j || data.lcm.(i).(j) cmv.(i) cmv.(j))) in

  let fuv = Array.init nb_cond (fun i ->
 (* not data.puv.(i) || Array.for_all (fun b -> b) pum.(i)) in *) (* 4.03.0 *)
    not data.puv.(i) || Array.fold_left (&&) true pum.(i)) in

  (* let launch = Array.for_all (fun b -> b) fuv in *) (* 4.03.0 only *)
  let launch = Array.fold_left (&&) true fuv in

  if launch then Printf.printf "YES\n"
  else Printf.printf "NO\n"

let _ =
  decide ()
